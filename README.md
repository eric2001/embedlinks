# EmbedLinks
![EmbedLinks](./screenshot.jpg) 

## Description
EmbedLinks is a plugin for [Gallery 3](http://gallery.menalto.com/) which will display BBCode and HTML links on each album and photo page that a visitor may then copy and paste into other sites in order to link to that album/photo/movie.  

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/88322).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

Once activated, you'll have an Admin > Settings > EmbedLinks menu option.  The following configuration options are available:
- **Show HTML Links:**  If checked the module will display HTML code to link to the current page.
- **Show BBCode Links:**  If checked the module will display BBCode to link to the current page.
- **Show the full URL:**  If checked the module will display the URL to the current page.
- **Show Links In The Actual Page:**  If checked, the fields will be displayed in the main window.
- **Display toolbar image for HTML/BBCode/URL links:**  If checked a "link" icon will be displayed at the top of the sidebar to open a pop-up window that will contain the links.

Additionally, an optional EmbedLinks sidebar is available with buttons that open a pop-up window to display HTML / BBCode / URLs.

## History
**Version 3.4.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Replaced logic for embeding flowplayer on movie pages with logic to generate html5 video tags.
> - Released 29 August 2021.
>
> Download: [Version 3.4.0](/uploads/ce67d09002494bf99baa6b574045537e/embedlinks340.zip)

**Version 3.3.0:**
> - Updated module.info for changes in Gallery 3.0.2.
> - Added a toolbar icon to a dialog window for HTML/BBcode and URLs.
> - Fixed the flowplayer embed code to work properly with the latest version of Gallery 3.
> - Updated the flowplayer embed code to support the videos module.
> - Released 05 August 2011.
>
> Download: [Version 3.3.0](/uploads/88fe346ff5c46c4ebaf75e8962b0c0ad/embedlinks330.zip)

**Version 3.2.0:**
> - Updated for Gallery 3, RC-1 Support.
> - Removed checkbox for enabling / disabling sidebar buttons.  This can now be controlled via Gallery's sidebar management screen.
> - Released 25 February 2010.
>
> Download: [Version 3.2.0](/uploads/019ef56804d889a1ba86e04dd72913d8/embedlinks320.zip)

**Version 3.1.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Updated for the new sidebar code in Gallery 3 Git
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 12 October 2009
>
> Download: [Version 3.1.0](/uploads/70e7128a6b2fc0383d2bb931731a05f3/embedlinks310.zip)

**Version 3.0.0:**
> - Added video support.
> - Fixed a translation bug on the admin page.
> - Released 24 August 2009
>
> Download: [Version 3.0.0](/uploads/ecad2c6da76519196ff8761a65a8e070/embedlinks300.zip)

**Version 2.3.0:**
> - Added an option to display the URLs without the HTML/BBCode.
> - Released 02 August 2009
>
> Download: [Version 2.3.0](/uploads/47c457f0e8f4d7cc4ad99aec214b93de/embedlinks230.zip)

**Version 2.2.0:**
> - Includes bharat's changes for the recent API update.
> - Released 31 July 2009
>
> Download: [Version 2.2.0](/uploads/96ade3b96ae19e185cf22eb5e366f254/embedlinks220.zip)

**Version 2.1.0:**
> - Changed the textareas over to input boxes.
> - Added some CSS code to make the input boxes larger.
> - Added some javascript code to automatically select the contents of the input boxes.
> - Released 25 July 2009.
>
> Download: [Version 2.1.0](/uploads/2680d9f5dc6d0013990a154a3688882c/embedlinks210.zip)

**Version 2.0.0:**
> - Now includes BBCode in addition to HTML links.
> - Now it will only display links to a fullsized version of a photo if the current visitor actually has the necessary privileges to view it.
> - Added an admin page to switch displaying of HTML and BBCode on and off (so if you don't want either type you can disable it).
> - Added code to display links in a dialog window (similar to how the exif plugin works).
> - Added an admin page to switch between displaying link codes on the page (how the previous version worked) or in a dialog.
> - Released 12 July 2009.
>
> Download: [Version 2.0.0](/uploads/3aaaf61a3db1fd5e40277951ac66a8be/embedlinks200.zip)

**Version 1.0.1:**
> - Big Fix:  Fixed an issue in "views/embedlinks_photo_block.html.php" that caused it to not work properly if there is a "'" in the URL.
> - Released on 12 June 2009
>
> Download: [Version 1.0.1](/uploads/0c4dad3cc6da5d0cd7a5e4bf014a6817/embedlinks101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 11 June 2009
>
> Download: [Version 1.0.0](/uploads/d36d604e81742e1a015cbe6298807b8c/embedlinks100.zip)
